%before the calculation three tables need to be imported manually (using
%the "import data" tool). If the bundled Fiji macro is used to generate
%the ellipsoid information, the three import files should be 
%the lamina position file ('lamina positions.txt'), the original ...
%ellipsoid file ('Ellipsoid.csv') and 
%the coordinate correction file ('subimage positions.txt') to adjust...
%ellipsoid positions to the 
%entire image

%store the lamina position in a matrix
L_lamina=max(size(laminapositions));
lamina_position=zeros(L_lamina, 3); 
%convert pixel unit to metric unit (adapt according to image metadata)
for i=1:L_lamina
   lamina_position(i,1)=laminapositions(i,1)*0.24;
   lamina_position(i,2)=laminapositions(i,2)*0.24;
   lamina_position(i,3)=laminapositions(i,3)*0.13;
end
lamina_x=lamina_position(:,1);
lamina_y=lamina_position(:,2);
lamina_z=lamina_position(:,3);
T=table(lamina_x,lamina_y,lamina_z);
writetable(T,'lamina_coordinates.csv');

%incorporate the subimage row and column information for each object...
%adjust the object coordinates to the entire image
%column and row data read as arrays but not matrix
%ellipsoid read as arrays but not matrix
L_cells=length(Cxpix);
Cxpix_column=zeros(L_cells,1);
Cypix_row=zeros(L_cells,1);
object_count=0;
start_count=0;
subimage_count=1;
i=1;
while i<L_cells
    if numberofobject(subimage_count)~=0
        Cxpix_column(i)=column(subimage_count);
        Cypix_row(i)=row(subimage_count);
        object_count=object_count+1;
        if object_count-start_count==numberofobject(subimage_count)
            subimage_count=subimage_count+1;
            start_count=object_count;
        end
        i=i+1;
    else
        subimage_count=subimage_count+1;
    end
end
cell_position=zeros(L_cells,3);
for i=1:L_cells
    %correct the real coordinates of the centroids
    cell_position(i,1)=0.24*(Cxpix(i)+100*Cxpix_column(i));
    cell_position(i,2)=0.24*(Cypix(i)+100*Cypix_row(i));
    cell_position(i,3)=0.13*(Czpix(i));
end
%write the centroid coordinates into a csv file
centroid_x=cell_position(:,1);
centroid_y=cell_position(:,2);
centroid_z=cell_position(:,3);
T=table(centroid_x,centroid_y,centroid_z);
writetable(T,'centroids.csv');

%find the closest lamina positions and calculate the angles
L_cells=length(Cxpix);
anglesummary=zeros(L_cells,6); %remember the centroids and angles
displacements=zeros(L_cells,12);
for k=1:L_cells
    cx=centroid_x(k);
    cy=centroid_y(k);
    cz=centroid_z(k);
    closest_lamina=distance(cx,cy,cz,lamina_x,lamina_y,lamina_z);
    angles=anglemeasurement(k,closest_lamina,Vxpix,Vypix,Vzpix);
    displacement=vectordisplacement(k,closest_lamina,Vxpix,Vypix,Vzpix);
    anglesummary(k,1)=cx;
    anglesummary(k,2)=cy;
    anglesummary(k,3)=cz;
    anglesummary(k,4)=angles(1);
    anglesummary(k,5)=angles(2);
    anglesummary(k,6)=angles(3);
    displacements(k,1)=cx;
    displacements(k,2)=cy;
    displacements(k,3)=cz;
    displacements(k,4)=displacement(1);
    displacements(k,5)=displacement(2);
    displacements(k,6)=displacement(3);
    displacements(k,7:9)=displacement(4:6);
    displacements(k,10:12)=displacement(7:9);
end
T=table(anglesummary);
writetable(T,'angleresults.csv');
T=table(displacements);
writetable(T,'displacements.csv');
