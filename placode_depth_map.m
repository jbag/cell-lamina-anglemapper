%this script is to be used after 'angel_calculation_main.m'
%when data are still stored backstage
%otherwise can import data again as written in 'angel_calculation_main.m'
%for the best result, the depth data need to be normalised (line 40-41)
%to get rid of exceptional points
x=lamina_x;
y=lamina_y;
z=lamina_z;
DT = delaunayTriangulation(x,y); 
corrected_depth=zeros(length(x),1);
%find out how many points per row
row=y(1);
count=1;
while y(count+1)==row
    count=count+1;
end
for i=1:length(y)
    if rem(i,count)==1 %start the calculation
        rowlevel=z(i:(i+count-1));
        epilevel=min(rowlevel);
    end
    if rem(i,count)==0 %end of the row
        for j=(i-count+1):i
            corrected_depth(j)=z(j)-epilevel;
        end
    end
end

[v,c]=voronoin([x(:),y(:)]);
figure();
for i=1:length(x)
    face=c{i}; %convert cell to matrix
    p=0;
    for j=1:length(face)
        if face(j)==1
            p=1;
        end
    end
  %  for j=1:length(face)
  %      if v(face(j),1)<=0 | v(face(j),2)<=0 | v(face(j),1)>=200 ...
   %             | v(face(j),2)>=200 % get rid of weird spikes at the edge
    %        p=1;
     %   end
    %end
    if p==0
         vertices=zeros(length(face),2);
         for j=1:length(face)
             vertices(j,:)=v(face(j),:);
         end
         face=1:length(face);
         mindepth=min(corrected_depth);
         maxdepth=max(corrected_depth);
         colour=[(corrected_depth(i)-mindepth)/(maxdepth-mindepth) 1 ...
             1];      
         hPolygon = patch('Faces',face,'Vertices',vertices,...
             'FaceColor',rgb2hsv(colour),'Edgecolor', 'none'); 
         %hold on
    end
end
