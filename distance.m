%calculate the closest points to the object and return the coordinates
function closest=distance(cx,cy,cz,lamina_x,lamina_y,lamina_z)
object_x=cx;
object_y=cy;
object_z=cz;
L=length(lamina_x);
distance_result=zeros(L,4);
for i=1:L
    distance_result(i,1)=sqrt((object_x-lamina_x(i))^2+(object_y-lamina_y(i))^2+...
        (object_z-lamina_z(i))^2);
    distance_result(i,2)=lamina_x(i);
    distance_result(i,3)=lamina_y(i);
    distance_result(i,4)=lamina_z(i);
end
sort_distance=sortrows(distance_result);
closest=zeros(18,4);
for j=1:18
    closest(j,1)=sort_distance(j,1);
    closest(j,2)=sort_distance(j,2);
    closest(j,3)=sort_distance(j,3);
    closest(j,4)=sort_distance(j,4);
end
end