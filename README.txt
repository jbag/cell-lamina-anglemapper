This code set consists of
1. Fiji (ImageJ) macros used to segment confocal image stacks of epithelia stained for cell outlines (beta-catenin), nuclei (DAPI) and the basal lamina (laminin)
2. MATLAB scripts to calculate the inclination (angle) between each segmented cell axis
and the local lamina. 

It was developed by Drs. Jingjing Li and Andrew Economou in the Jeremy Green group at King's College London to analyse cells of an invaginating salivary gland placode (published in Jingjing Li, Andrew D. Economou, Barbara Vacca and Jeremy B.A. Green (2020) "Epithelial invagination by vertical telescoping", Nature Communications).

Main MATLAB scripts are:
'angle_calculation_main.m' to compute the angle using segmented object and lamina
data exported from Fiji (refer to the fiji script)
'quivermap_object.m' to draw a vector map of the cells relative to the local lamina
(showing inclination)
'quivermap_lamina.m' to draw a vector map of the lamina as local normal vectors 
'placode_depth_map.m' to draw a heatmap based on the depth of the placode to show 
where it is in the entire image, to compare with the vector maps

Functions are:
'anglemeasurement.m'
'distance.m'
'vectordisplacement.m'
