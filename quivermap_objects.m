%import 'displacement.csv' manually
%vector map of the objects (by displacement between object and lamina)
figure();
for i=1:length(displacements)
    x=displacements(i,1);
    y=displacements(i,2);
    u=displacements(i,4);
    v=displacements(i,5);
    q=quiver(x,y,u,v);
    if u<=0
        q.Color='red';
    else
        q.Color='blue';
    end
    q.AutoScaleFactor=10;
    q.MaxHeadSize=1;
    q.LineWidth=1;
    hold on
end
title('vector map of displacement between cells and lamina');