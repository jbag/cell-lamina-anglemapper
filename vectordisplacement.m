%calculate the normal vector to a plane determined by three points
%and return the vector
function displacement=vectordisplacement(k,closest,Vxpix,Vypix,Vzpix)
a1=zeros(1,3);
a2=zeros(1,3);
a3=zeros(1,3);
a4=zeros(1,3);
a5=zeros(1,3);
a6=zeros(1,3);
b1=zeros(1,3);
b2=zeros(1,3);
b3=zeros(1,3);
b4=zeros(1,3);
b5=zeros(1,3);
b6=zeros(1,3);
a1(1)=closest(1,2)-closest(3,2);
a1(2)=closest(1,3)-closest(3,3);
a1(3)=closest(1,4)-closest(3,4);
b1(1)=closest(2,2)-closest(3,2);
b1(2)=closest(2,3)-closest(3,3);
b1(3)=closest(2,4)-closest(3,4);
a2(1)=closest(4,2)-closest(6,2);
a2(2)=closest(4,3)-closest(6,3);
a2(3)=closest(4,4)-closest(6,4);
b2(1)=closest(5,2)-closest(6,2);
b2(2)=closest(5,3)-closest(6,3);
b2(3)=closest(5,4)-closest(6,4);
a3(1)=closest(7,2)-closest(9,2);
a3(2)=closest(7,3)-closest(9,3);
a3(3)=closest(7,4)-closest(9,4);
b3(1)=closest(8,2)-closest(9,2);
b3(2)=closest(8,3)-closest(9,3);
b3(3)=closest(8,4)-closest(9,4);
a4(1)=closest(10,2)-closest(12,2);
a4(2)=closest(10,3)-closest(12,3);
a4(3)=closest(10,4)-closest(12,4);
b4(1)=closest(11,2)-closest(12,2);
b4(2)=closest(11,3)-closest(12,3);
b4(3)=closest(11,4)-closest(12,4);
a5(1)=closest(13,2)-closest(15,2);
a5(2)=closest(13,3)-closest(15,3);
a5(3)=closest(13,4)-closest(15,4);
b5(1)=closest(14,2)-closest(15,2);
b5(2)=closest(14,3)-closest(15,3);
b5(3)=closest(14,4)-closest(15,4);
a6(1)=closest(16,2)-closest(18,2);
a6(2)=closest(16,3)-closest(18,3);
a6(3)=closest(16,4)-closest(18,4);
b6(1)=closest(17,2)-closest(18,2);
b6(2)=closest(17,3)-closest(18,3);
b6(3)=closest(17,4)-closest(18,4);
C1=cross(a1,b1);
C1=C1/norm(C1);
if C1(3)>0
    C1=(-1)*C1;
end
C2=cross(a2,b2);
C2=C2/norm(C2);
if C2(3)>0
    C2=(-1)*C2;
end
C3=cross(a3,b3);
C3=C3/norm(C3);
if C3(3)>0
    C3=(-1)*C3;
end
C4=cross(a4,b4);
C4=C4/norm(C4);
if C4(3)>0
    C4=(-1)*C4;
end
C5=cross(a5,b5);
C5=C5/norm(C5);
if C5(3)>0
    C5=(-1)*C5;
end
C6=cross(a6,b6);
C6=C6/norm(C6);
if C6(3)>0
    C6=(-1)*C6;
end
C_total_matrix=[C1;C2;C3;C4;C5;C6];
C_total=[0 0 0];
count_vector=0;
for C_count=1:6
    is_not_nan=isnan(C_total_matrix(C_count,1));
    if is_not_nan==0
        C_total=C_total+C_total_matrix(C_count,:);
        count_vector=count_vector+1;
    end
end
C_average=(1/6)*C_total;
%if C_average(1)<0.1 | C_average(2)<0.1 | C_average(3)<0.1
 %   C_average=(1/2)*(C1+C2);
%end
%calculate the displacement between the object major axis and the plane vector
lamina=C_average/norm(C_average);
object=zeros(1,3);
object(1)=Vxpix(k);
object(2)=Vypix(k);
object(3)=Vzpix(k);
displacement(1:3)=(object-lamina)/norm(object-lamina);
displacement(4:6)=object;
displacement(7:9)=lamina;
end