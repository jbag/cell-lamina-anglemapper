//this macro divides the big image into any size of small tiles
//for 1) basal lamina recognition and 2) segmentation
//need to divide twice for two purposes
//for lamina positions, use width/height 20, displacement 20
//for segmentation, use width/height 400, displacement 200

//if stacks were taken bottom to top, need to reverse before processing
macro "substack"
{
	dir=getDirectory("Choose a Directory");
	substack(dir);
}

//this macro finds the upper and lower bounds of the epithelium
macro "bounds"
{
	dir=getDirectory("Choose a Directory");
	bounds(dir);
}

//This macro registers the positions of the basal lamina in a 20 pixel interval
//this mesh of coordinates will be used for measuring the angle between cell and lamina
//double check before running this macro DAPI is C1, catenin is C2 and laminin is C3
macro "lamina"
{
	dir=getDirectory("Choose a Directory");
	LaminaPositions(dir);
}

//this macro trims below the basal lamina in each tile to clean mesenchymal signals for segmentation
macro "Trimbelowlamina"
{
	dir=getDirectory("Choose a Directory");
	Trimbelowlamina(dir);	
}

//this macro combines trimmed tiles into a big image for segmentation
macro "combination1"
{
	dir=getDirectory("Choose a Directory");
	CombinedEpithelium(dir);
}

//this macro finds local maxima and segment cells based on iterative thresholding
macro "Segmentation"
{
	dir=getDirectory("choose a directory");
	Segmentation(dir);
}

//divide the big stack into small substacks and save them in the /Stacks directory
function substack(dir)
{
	imagename=getTitle();
	selectWindow(imagename);
	run("Select None");
	File.makeDirectory(dir+"SubStacks/");
	dir_substack=dir+"SubStacks/";	
	Dialog.create("Selection");
	Dialog.addCheckbox("Upload selection?", false);//an output of the function is to record 
	//the details of the way the image divided into a grid so can be repeated by uploading subsequently
	Dialog.show();
	selection=Dialog.getCheckbox();
	if(selection==false)
	{
	// doing for first time, therefore need to input a stack name, a tile width and a displacement 
	//(which together define the size of the overlap between adjacent tiles)
		col=0;
		row=0;
		title="substacks";
		Dialog.create("ROI");
		Dialog.addString("Filename:", title);
		Dialog.addNumber("Width:", 20);
		Dialog.addNumber("Height:", 20);
		Dialog.addNumber("X displacement:", 20);
		Dialog.addNumber("Y displacement:", 20);
		Dialog.show();
		w1=Dialog.getNumber();
		h1=Dialog.getNumber();
		w2=Dialog.getNumber();
		h2=Dialog.getNumber();
		title=Dialog.getString();
		getSelectionBounds(x,y,width,height);
	}
	else
	{
		// read the details out of the details text file
		Dialog.create("Start point");
		//can make substacks starting at a particular tile if need have been deleted by segmentation 
		//function but need to repeat
		Dialog.addNumber("Column", 0);
		Dialog.addNumber("Row", 0);	
		Dialog.show();
		col=Dialog.getNumber();
		row=Dialog.getNumber();
		detailsfile=File.openAsString(dir+"details.txt");
		details=split(detailsfile,"\n");
		detailsvalues=split(details[1],"\t");
		name=detailsvalues[0];
		x=detailsvalues[1];
		y=detailsvalues[2];
		width=detailsvalues[3];
		height=detailsvalues[4];
		title=detailsvalues[5];
		w1=detailsvalues[6];
		h1=detailsvalues[7];
		w2=detailsvalues[8];
		h2=detailsvalues[9];
		if(name!=getTitle())
		{
			Dialog.create("Warning");
			Dialog.addMessage("Images not match");
			Dialog.show();
		}
	}
	w=round(width/w2);
	h=round(height/h2);
	wD=NumberOfDigits(w);
	hD=NumberOfDigits(h);
	setBatchMode(true);
	timer=TimerA("Subdividing stack",true);
	timertotal=h*w;	
	current=1;
	f=File.open(dir+"substack positions.txt");
	print(f,"image\trow\tcolumn");
	for (j=0; j<h; j++) 
	{
		for (i=0; i<w; i++) 
		{
			if(((i==col)&(j>=row))|(i>col))
			{	
				//sets start point
				makeRectangle(x+w2*i,y+h2*j,w1,h1);
				//to allow list of tiles to be in correct order, file names consist of same number of digits 
				I=Digits(i,wD);
				J=Digits(j,hD);
				name=title+"-"+J+"-"+I+".tif";//names need to have correct number of digits 
				//so when ranked in a directory appear in numerical order (had problems with 
				//different length numbers of different absolute length not appearing in the correct order)
				n=nSlices();
				run("Duplicate...", "title=[&name] duplicate range=1-&n");
				save(dir_substack+name);
				close();
				TimerB("Subdividing stack",timer,current,timertotal);
				current=current+1;
				print(f, name+"\t"+j+"\t"+i); //remember the positions of subimages
			}
		}
	}
	File.close(f);
	if(selection==false)
	{
		// output details of stack not retiling an image
		name=getTitle();
		f=File.open(dir+"details.txt");
		print(f, "Image\tX\tY\tWidth\tHeight\tSample name\tSample width\tSample height\tX displacement\tY displacement");
		print(f, name+"\t"+x+"\t"+y+"\t"+width+"\t"+height+"\t"+title+"\t"+w1+"\t"+h1+"\t"+w2+"\t"+h2);
	}
	TimerC("Subdividing stack");
	setBatchMode(false);
}

function bounds(dir)
{
	dir1=dir+"SubStacks/";  
	File.makeDirectory(dir+"ModifiedStacks/");
	dir2=dir+"ModifiedStacks/";
	list=getFileList(dir1); 
	//first set up arrays the length of the number of tiles to hold values for each tile
	foundupper=newArray(list.length); //array holding true/false for if upper slice found by minimum/inflection
	US=newArray(list.length); //value of the upper slice as position in stack
	LS=newArray(list.length); //string of the lower slice possible minima as positions in stack
	//LSI=newArray(list.length); //string of associated intensities for each minimum in LS
	//LI=newArray(list.length); //string of inflections as positions in stack
	//LII=newArray(list.length); //string of associated intensities for each inflection in LS
	//L=newArray(list.length); //value of the lower slice as position in stack ie slice from LS or LI which meets criteria
	//StartSlice=newArray(list.length); //slice number where image begins (incase image stops midway through stack)
	//FinishSlice=newArray(list.length); //slice number where image ends (incase image stops midway through stack)
	//NotBlankStack=newArray(list.length); //true or false for whether stack contains image ie marks up slices past medial edge so they are ignored by subsequent steps
	setBatchMode(true);
	timer=TimerA("trimming stacks",true);
	timertotal=list.length;
	f=File.open(dir+"upper and lower positions.txt");
	print(f, "Image"+"\t"+"upper"+"\t"+"lower"+"\n");
	current=1;
	for (i=0; i<list.length; i++) 
	{  
		//processes each stack in /Stacks directory 
		//print(((i/(list.length))*100),"%");
		open(dir1+list[i]);
		name=getTitle();
		rename("stack");
		width=getWidth();
		height=getHeight();
		run("Split Channels");
		selectWindow("C1-stack");
		count=nSlices;
		intensityBlue=newArray(count);
		intensityGreen=newArray(count);
		intensityRed=newArray(count);
		intensityTotal=newArray(count);		
		for (j=0; j<count; j++)
		{
			selectWindow("C1-stack");
			setSlice(j+1);
			makeRectangle(0, 0, width, height);
			getRawStatistics(nPixels, mean, min, max, std, histogram);
			intensityBlue[j]=mean;
			selectWindow("C2-stack");
			setSlice(j+1);
			makeRectangle(0, 0, width, height);
			getRawStatistics(nPixels, mean, min, max, std, histogram);
			intensityGreen[j]=mean;
			selectWindow("C3-stack");
			setSlice(j+1);
			makeRectangle(0, 0, width, height);
			getRawStatistics(nPixels, mean, min, max, std, histogram);
			intensityRed[j]=mean;
			intensityTotal[j]=intensityBlue[j]+intensityGreen[j]+intensityRed[j];
			print("mean total intensity of this slice",intensityTotal[j]);
		}
		//Array.getStatistics(intensityTotal,min,max,mean,stDev);	
		for (j=0; j<count; j++)
		{
			if(intensityTotal[j]>30)//original value was set to 30 in SG
			{
				foundupper[i]=1;
				US[i]=j+1;
				j=count;
			}
		}
		print("upper slice",US[i]);
		Array.getStatistics(intensityRed,min,max,mean,stDev);	
		for (j=count-1; j>=0; j--)
		{
			if(intensityRed[j]>40)//original value was set to 40 in SG
			{
				LS[i]=j+1;
				j=-1;
			}
		}		
		print("lower slice",LS[i]);
		//black out slices lower than the lamina and higher than the surface of the epithelium
		selectWindow("C1-stack");
		for(k=US[i];k>=1;k--)
		{
			setSlice(k);
			run("Select All");
			setForegroundColor(0, 0, 0);
			run("Fill", "slice");
		}
		for(k=LS[i];k<=count;k++)
		{
			setSlice(k);
			run("Select All");
			setForegroundColor(0, 0, 0);
			run("Fill", "slice");
		}
		selectWindow("C2-stack");
		for(k=US[i];k>=1;k--)
		{
			setSlice(k);
			run("Select All");
			setForegroundColor(0, 0, 0);
			run("Fill", "slice");
		}
		for(k=LS[i];k<=count;k++)
		{
			setSlice(k);
			run("Select All");
			setForegroundColor(0, 0, 0);
			run("Fill", "slice");
		}
		selectWindow("C3-stack");
		for(k=US[i];k>=1;k--)
		{
			setSlice(k);
			run("Select All");
			setForegroundColor(0, 0, 0);
			run("Fill", "slice");
		}
		for(k=LS[i];k<=count;k++)
		{
			setSlice(k);
			run("Select All");
			setForegroundColor(0, 0, 0);
			run("Fill", "slice");
		}
		run("Merge Channels...", "c1=C1-stack c2=C2-stack c3=C3-stack create");
		save(dir2+name);
		close();
		print(f, name+"\t"+US[i]+"\t"+LS[i]+"\n");
		TimerB("trimming stack",timer,current,timertotal);
		current=current+1;
	}
	TimerC("trimming stack");
	setBatchMode(false);
}

function LaminaPositions(dir)
{
	dir2=dir+"ModifiedStacks/";
	list=getFileList(dir2); 
	xposition=newArray(list.length);
	yposition=newArray(list.length);
	zposition=newArray(list.length);
	setBatchMode(true);
	timer=TimerA("finding lamina coordinates",true);
	timertotal=list.length;
	f=File.open(dir+"lamina positions.txt");
	print(f, "Image"+"\t"+"x"+"\t"+"y"+"\t"+"z"+"\n");
	current=1;
	SliceBounds=File.openAsString(dir+"upper and lower positions.txt");
	Bounds=split(SliceBounds,"\n");
	detailsfile=File.openAsString(dir+"details.txt");
	details=split(detailsfile,"\n");
	detailsvalues=split(details[1],"\t");
	entirewidth=parseFloat(detailsvalues[3]);
	entireheight=parseFloat(detailsvalues[4]);
	open(dir2+list[0]);
	name=getTitle();
	rename("stack");
	count=nSlices;
	count=count/3; //only need to copy in one channel
	selectWindow("stack");
	close();
	newImage("laminapositions", "8-bit black", entirewidth, entireheight, count);
	for (i=0; i<list.length; i++) 
	{  
		open(dir2+list[i]);
		name=getTitle();
		rename("stack");
		width=getWidth();
		height=getHeight();
		run("Split Channels");
		selectWindow("C1-stack");
		close();
		selectWindow("C2-stack");
		close();
		selectWindow("C3-stack");
		count=nSlices;
		upperlowervalues=split(Bounds[i+1],"\t");
		name=upperlowervalues[0];
		upper=parseFloat(upperlowervalues[1]);
		lower=parseFloat(upperlowervalues[2]);
		lower=lower-1;
		intensityRed=newArray(count);
		stdRed=newArray(count);
		for (j=0; j<count; j++)
		{
			selectWindow("C3-stack");
			setSlice(j+1);
			makeRectangle(0, 0, width, height);
			getRawStatistics(nPixels, mean, min, max, std, histogram);
			intensityRed[j]=mean;
			stdRed[j]=std;
		}		
		intensityRedsmooth=Smooth(intensityRed,upper,lower,2);
		gradientRed=Gradient(intensityRedsmooth,upper,lower,5,0);
		//curvatureRed=Gradient(gradientRed,upper,lower,5,1);
		Array.getStatistics(intensityRedsmooth,min,max,mean,stDev);
		/*for(j=lower;j>=upper;j--){
			if(intensityRedsmooth[j]==max){
				z=j;
				j=upper;
			}
		}*/
		for(j=lower;j>=upper;j--) //it finds the first peak of laminin intensity
		{
			//print("gradient",gradientRed[j]);
			if(gradientRed[j]>=0)
			{
				z=j;
				j=upper;
			}
		}
		/*for(j=lower;j>=upper;j--){
			if((gradientRed[j-1]>=0)&(gradientRed[j]<0)){
				if(stdRed[j]<60){
					z=j;
					j=upper;
				}
			}
		}*/
		//calculate the coordinates of the middle of the lamina in small tiles
		totalcolumn=floor(entirewidth/20);
		totalrow=floor(entireheight/20);
		row=floor(i/totalcolumn);
		column=i-row*totalcolumn;		
		y=row*20+height/2;
		x=column*20+width/2;
		xposition[i]=x;
		yposition[i]=y;
		zposition[i]=z;
		//need a correction step here so will only store the coordinates in arrays not in text file
		//print(f, name+"\t"+x+"\t"+y+"\t"+z+"\n");
		/*selectWindow("laminapositions");
		setSlice(z);
		makeOval(x,y,5,5);
		setForegroundColor(255, 255, 255);
		run("Fill", "slice");*/
		TimerB("finding lamina coordinates",timer,current,timertotal);
		current=current+1;
		selectWindow("C3-stack");
		close();
	}
	//find out off range positions and correct them
	for (i=0; i<list.length; i++)
	{
		if((xposition[i]>10)&(xposition[i]<(entirewidth-29))&(yposition[i]>10)&(yposition[i]<(entireheight-29)))
		{
			//set the right and bottom margin width/height-29 when the division of width/20 has a non-zero residue
			up=zposition[i]-zposition[i-totalcolumn];
			left=zposition[i]-zposition[i-1];
			right=zposition[i]-zposition[i+1];
			down=zposition[i]-zposition[i+totalcolumn];
			//upleft=zposition[i]-zposition[i-39];
			//upright=zposition[i]-zposition[i-37];
			//downleft=zposition[i]-zposition[i+37];
			//downright=zposition[i]-zposition[i+39];
			/*smallest=zposition[i-totalcolumn];
			correctposition=smallest;
			if(zposition[i-1]<smallest)
			{
				smallest=zposition[i-1];
				correctposition=zposition[i-1];
			}
			if(zposition[i+1]<smallest)
			{
				smallest=zposition[i+1];
				correctposition=zposition[i+1];
			}
			if(zposition[i+totalcolumn]<smallest)
			{
				smallest=zposition[i+totalcolumn];
				correctposition=zposition[i+totalcolumn];
			}*/
			//this method of lamina position calculation tends to lower the real positions in z
			//therefore although theoretically the modulus of the difference should be used, normally it will be positive
			//if((up>50)|(left>50)|(right>50)|(down>50)/*|(upleft>50)|(upright>50)|(downleft>50)|(downright>50)*/)
			//{
			//	zposition[i]=(zposition[i-1]+zposition[i+1]+zposition[i-totalcolumn]+zposition[i+totalcolumn])/4;
			//}
			if((up>50)|(left>50)|(right>50)|(down>50)/*|(upleft>50)|(upright>50)|(downleft>50)|(downright>50)*/)
			{
				zposition[i]=(zposition[i-1]+zposition[i+1])/2;
			}
			/*if(((zposition[i]-zposition[i-1])>30)&((zposition[i]-zposition[i+1])>30))
			{
				zposition[i]=(zposition[i-1]+zposition[i+1])/2;
			}
			if(((zposition[i]-zposition[i-totalcolumn])>30)&((zposition[i]-zposition[i+totalcolumn])>30))
			{
				zposition[i]=(zposition[i-totalcolumn]+zposition[i+totalcolumn])/2;
			}*/
			/*if(((zposition[i]-zposition[i-38*2])<-50)|((zposition[i]-zposition[i+38*2])<-50)){
				if((zposition[i]-zposition[i-38])<-30){
					zposition[i]=zposition[i-38];
				}
				if((zposition[i]-zposition[i+38])<-30){
					zposition[i]=zposition[i+38];
				}
			}*/		
		}
		x=xposition[i];
		y=yposition[i];
		z=zposition[i];	
	}
	//double check points that are two high in z
	/*for (i=0; i<list.length; i++){
		if((xposition[i]>10)&(xposition[i]<1010)&(yposition[i]>10)&(yposition[i]<750)){
			if(((zposition[i]-zposition[i-38])>50)|((zposition[i]-zposition[i-38])<-50)){
				zposition[i]=zposition[i-38];
			}	
		}				
		x=xposition[i];
		y=yposition[i];
		z=zposition[i];	
	}*/
	//deal with edge points
	for (i=0; i<list.length; i++)
	{
		if((xposition[i]>30)&(xposition[i]<(entirewidth-49))&(yposition[i]>30)&(yposition[i]<(entireheight-49)))
		{
			up=zposition[i]-zposition[i-totalcolumn];
			down=zposition[i]-zposition[i+totalcolumn];
			if((up>50)|(down>50))
			{
				zposition[i]=(zposition[i-totalcolumn]+zposition[i+totalcolumn])/2;
			}
		}
	}
	for (i=0; i<list.length; i++)
	{
		if((xposition[i]<=10)&(yposition[i]<=10)) //topleft corner
		{
			if((zposition[i]-zposition[i+totalcolumn+1])>50)
			{
				zposition[i]=zposition[i+totalcolumn+1];
			}
		}
		if((xposition[i]>=(entirewidth-29))&(yposition[i]<=10)) //topright corner
		{
			if((zposition[i]-zposition[i+totalcolumn-1])>50)
			{
				zposition[i]=zposition[i+totalcolumn-1];
			}
		}
		if((xposition[i]<=10)&(yposition[i]>=(entireheight-29))) //bottomleft corner
		{
			if((zposition[i]-zposition[i-totalcolumn+1])>50)
			{
				zposition[i]=zposition[i-totalcolumn+1];
			}
		}
		if((xposition[i]>=(entirewidth-29))&(yposition[i]>=(entireheight-29))) //bottomright corner
		{
			if((zposition[i]-zposition[i-totalcolumn-1])>50)
			{
				zposition[i]=zposition[i-totalcolumn-1];
			}
		}
		if((xposition[i]<=10)&(yposition[i]>10)&(yposition[i]<(entireheight-29)))
		{
			if((zposition[i]-zposition[i+1])>50)
			{
				zposition[i]=zposition[i+1];
			}
		}
		if((xposition[i]>=entirewidth-29)&(yposition[i]>10)&(yposition[i]<(entireheight-29)))
		{
			if((zposition[i]-zposition[i-1])>50)
			{
				zposition[i]=zposition[i-1];
			}
		}
		if((yposition[i]<=10)&(xposition[i]>10)&(xposition[i]<(entirewidth-29)))
		{
			if((zposition[i]-zposition[i+totalcolumn])>50)
			{
				zposition[i]=zposition[i+1];
			}
		}
		if((yposition[i]>=entireheight-29)&(xposition[i]>10)&(xposition[i]<(entirewidth-29)))
		{
			if((zposition[i]-zposition[i-totalcolumn])>50)
			{
				zposition[i]=zposition[i-1];
			}
		}	
		x=xposition[i];
		y=yposition[i];
		z=zposition[i];	
		upperlowervalues=split(Bounds[i+1],"\t");
		name=upperlowervalues[0];
		print(f, name+"\t"+x+"\t"+y+"\t"+z+"\n");
		selectWindow("laminapositions");
		setSlice(z);
		makeOval(x,y,5,5);
		setForegroundColor(255, 255, 255);
		run("Fill", "slice");
	}
	selectWindow("laminapositions");
	saveAs("tiff",dir+"laminapositions.tif");
	TimerC("finding lamina coordinates");
	setBatchMode(false);
}

function Trimbelowlamina(dir)
{
	dir1=dir+"ModifiedStacks/";
	list=getFileList(dir1); 
	LS=newArray(list.length);
	setBatchMode(true);
	timer=TimerA("trimming stacks",true);
	timertotal=list.length;
	laminapositions=File.openAsString(dir+"lamina positions.txt");
	positions=split(laminapositions,"\n");
	SliceBounds=File.openAsString(dir+"upper and lower positions.txt");
	Bounds=split(SliceBounds,"\n");
	current=1;
	for (i=0; i<list.length; i++)
	{
		positionvalues=split(positions[i+1],"\t");
		x=parseFloat(positionvalues[1]);
		y=parseFloat(positionvalues[2]);
		z=parseFloat(positionvalues[3]);
		open(dir1+list[i]);
		name=getTitle();
		rename("stack");
		width=getWidth();
		height=getHeight();
		run("Split Channels");
		count=nSlices;
		//lowerfill=0;		
		for(j=z+1;j<=count;j++)
		{
			selectWindow("C1-stack");
			setSlice(j);
			run("Select All");
			setForegroundColor(0, 0, 0);
			run("Fill", "slice");
			/*if (lowerfill<20)
			{
				selectWindow("C2-stack");
				setSlice(j);
				run("Select All");
				setForegroundColor(255, 255, 255);
				run("Fill", "slice");
				lowerfill=lowerfill+1;
			}
			else
			{*/
				selectWindow("C2-stack");	
				setSlice(j);
				run("Select All");
				setForegroundColor(0, 0, 0);
				run("Fill", "slice");
			//}		
			selectWindow("C3-stack");
			setSlice(j);
			run("Select All");
			setForegroundColor(0, 0, 0);
			run("Fill", "slice");
		}
		/*upperlowervalues=split(Bounds[i+1],"\t");
		name=upperlowervalues[0];
		upper=parseFloat(upperlowervalues[1]);
		lower=parseFloat(upperlowervalues[2]);
		for(j=upper;j<(upper+20);j++)
		{
			selectWindow("C2-stack");
			setSlice(j);
			run("Select All");
			setForegroundColor(255, 255, 255);
			run("Fill", "slice");
		}*/
		run("Merge Channels...", "c1=C1-stack c2=C2-stack c3=C3-stack create");
		save(dir1+name);
		close();
		TimerB("trimming stack",timer,current,timertotal);
		current=current+1;		
	}	
	TimerC("trimming stack");
	setBatchMode(false);
}

function CombinedEpithelium(dir)
{	
	dir1=dir+"ModifiedStacks/";
	File.makeDirectory(dir+"CombinedStacks/");
	dir2=dir+"CombinedStacks/";
	list1=getFileList(dir1); 
	setBatchMode(true);
	detailsfile=File.openAsString(dir+"details.txt");
	details=split(detailsfile,"\n");
	detailsvalues=split(details[1],"\t");
	entirewidth=parseFloat(detailsvalues[3]);
	entireheight=parseFloat(detailsvalues[4]);
	totalcolumn=floor(entirewidth/20);
	totalrow=floor(entireheight/20);
	for (i=0; i<list1.length; i++)
	{
		row=floor(i/totalcolumn);
		column=i-row*totalcolumn;	
		if((i==0)|(column==0))
		{ //need to put 39/38 into variables
			open(dir1+list1[i]);
			name=getTitle();
			rename("stack_1");
			i=i+1;
			open(dir1+list1[i]);
			name=getTitle();
			rename("stack_2");
			selectWindow("stack_1");
			run("Split Channels");
			selectWindow("stack_2");
			run("Split Channels");
			run("Combine...", "stack1=C1-stack_1 stack2=C1-stack_2");
			rename("C1-stack1");
			run("Combine...", "stack1=C2-stack_1 stack2=C2-stack_2");
			rename("C2-stack1");
			run("Combine...", "stack1=C3-stack_1 stack2=C3-stack_2");
			rename("C3-stack1");
		}
		i=i+1;
		row=floor(i/totalcolumn);
		column=i-row*totalcolumn;
		while(column!=0)
		{
			open(dir1+list1[i]);
			rename("stack_2");
			run("Split Channels");
			run("Combine...", "stack1=C1-stack1 stack2=C1-stack_2");
			rename("C1-stack1");
			run("Combine...", "stack1=C2-stack1 stack2=C2-stack_2");
			rename("C2-stack1");
			run("Combine...", "stack1=C3-stack1 stack2=C3-stack_2");
			rename("C3-stack1");
			i=i+1;
			row=floor(i/totalcolumn);
			column=i-row*totalcolumn;
		}
		run("Merge Channels...", "c1=[C1-stack1] c2=[C2-stack1] c3=[C3-stack1] create");
		rename("stack");
		title=getTitle();
		rownumber=Digits((row-1),2);//need to be able to change 2 according to image
		name=title+"-"+rownumber+".tif";
		save(dir2+name);
		close();
		i=i-1;	
	}
	list2=getFileList(dir2); 
	i=0;
	open(dir2+list2[i]);
	rename("stack_1");
	i=i+1;
	open(dir2+list2[i]);
	rename("stack_2");
	selectWindow("stack_1");
	run("Split Channels");
	selectWindow("stack_2");
	run("Split Channels");
	run("Combine...", "stack1=C1-stack_1 stack2=C1-stack_2 combine");
	rename("C1-stack1");
	run("Combine...", "stack1=C2-stack_1 stack2=C2-stack_2 combine");
	rename("C2-stack1");
	run("Combine...", "stack1=C3-stack_1 stack2=C3-stack_2 combine");
	rename("C3-stack1");
	i=i+1;
	while(i<(list2.length-1))
	{
		open(dir2+list2[i]);
		rename("stack_2");
		run("Split Channels");
		run("Combine...", "stack1=C1-stack1 stack2=C1-stack_2 combine");
		rename("C1-stack1");
		run("Combine...", "stack1=C2-stack1 stack2=C2-stack_2 combine");
		rename("C2-stack1");
		run("Combine...", "stack1=C3-stack1 stack2=C3-stack_2 combine");
		rename("C3-stack1");
		i=i+1;
	}
	open(dir2+list2[i]);
	rename("stack_2");
	run("Split Channels");
	run("Combine...", "stack1=C1-stack1 stack2=C1-stack_2 combine");
	rename("C1-stack1");
	run("Combine...", "stack1=C2-stack1 stack2=C2-stack_2 combine");
	rename("C2-stack1");
	run("Combine...", "stack1=C3-stack1 stack2=C3-stack_2 combine");
	rename("C3-stack1");
	run("Merge Channels...", "c1=[C1-stack1] c2=[C2-stack1] c3=[C3-stack1] create");
	rename("combination of trimmed grids");
	title=getTitle();
	name=title+".tif";
	save(dir+name);
	close();
	setBatchMode(false);
}

function Segmentation(dir)
{
	setBatchMode(true);
	dir1=dir+"SubStacks/";
	File.makeDirectory(dir+"segmented_images/");
	File.makeDirectory(dir+"segmented_rois/");
	dir2=dir+"segmented_images/";
	dir3=dir+"segmented_rois/";
	list1=getFileList(dir1);
	Dialog.create("restart midway");
	Dialog.addCheckbox("restart midway?", false);
	Dialog.show();
	stoppedmidway=Dialog.getCheckbox();
	if(stoppedmidway==false)
	{
		for (im=0;im<list1.length;im++)
		{
			open(dir1+list1[im]);
			name=getTitle();
			width=getWidth();
			height=getHeight();
			rename("stack");
			run("Split Channels");
			selectWindow("C1-stack");
			Z=nSlices();
			intensityBlue=newArray(Z);
			selectWindow("C1-stack");
			run("Smooth (3D)", "method=Gaussian sigma=1.000 use");
			selectWindow("Smoothed");
			//find dapi channel threshold
			//setSlice(Z/2);
			for (j=0; j<Z; j++)
			{
				selectWindow("Smoothed");
				setSlice(j+1);
				makeRectangle(0, 0, width, height);
				getRawStatistics(nPixels, mean, min, max, std, histogram);
				intensityBlue[j]=max;
			}
			//
			//adjust DAPI level
			//
			for (j=0; j<Z; j++)
			{
				if(intensityBlue[j]>50)
				{
					top=j;
					j=Z;
				}
			}
			for (j=Z-1; j>=0; j--)
			{
				if(intensityBlue[j]>50)
				{
					bottom=j;
					j=0;
				}
			}
			setSlice((top+bottom)/2);
			setAutoThreshold("Default dark");
			getThreshold(lower, upper);
			bluethreshold=lower;
			selectWindow("Smoothed");
			rename("smoothed blue");
			selectWindow("C1-stack");
			close();
			//find local minima with iterative thresholding and object counter
			imageCalculator("Add create stack", "C2-stack","C3-stack");
			rename("stack");
			selectWindow("C2-stack");
			close();
			selectWindow("C3-stack");
			close();
			selectWindow("stack");
			x=getHeight();
			y=getWidth();
			//run("Slice Keeper", "first="+top+"last="+bottom+"increment=1");//delete unnecessary slices
			//selectWindow("stack");
			//close();
			//selectWindow("stack kept stack");
			//rename("stack");
			z=nSlices();
			run("Invert", "stack");
			rename("stack");
			run("Smooth (3D)", "method=Gaussian sigma=1.000 use");
			selectWindow("stack");
			close();
			selectWindow("Smoothed");
			//run("Enhance Contrast...", "saturated=1 normalize process_all");
			//run("Enhance Contrast", "saturated=1 normalize update normalize_all");
			//as can be problems if no objects identified, stamp bottom slice with a 10x10 square in corner so not to interfer with subsequent processing
			setSlice(1);
			setColor(255,255,255);
			fillRect(0,0,10,10);
			areaz=z*x*y;
			//
			//adjust the thresholding starting point
			//
			i=60;
			threshold=220-i;
			print("threshold",threshold);
			i=i-2.5;
			selectWindow("Smoothed");
			run("3D Objects Counter", "threshold="+threshold+" slice=1 min.=100 max.="+areaz+" objects");
			selectWindow("Objects map of Smoothed");
			run("3D Manager");
			Ext.Manager3D_AddImage();
			Ext.Manager3D_Count(nb_obj);
			print("number of object",nb_obj);
			selectWindow("Objects map of Smoothed");
			close();
			obj_end=nb_obj;
			obj_middle=obj_end;
			//I used 5 in threshold stepping because I found the intensity still varies a bit after smoothing
			//5 misses some information so need to try smaller steps
			while(i>=0)
			{
				threshold=225-i;
				print("threshold",threshold);
				selectWindow("Smoothed");
				run("3D Objects Counter", "threshold="+threshold+" slice=1 min.=100 max.="+areaz+" objects");
				selectWindow("Objects map of Smoothed");
				Ext.Manager3D_AddImage();
				Ext.Manager3D_Count(nb_obj); 
				print("total number of objects",nb_obj);
				selectWindow("Objects map of Smoothed");
				close();
				obj_end=nb_obj; //end of object list
				new_obj=obj_end-obj_middle; //added new objects at this threshold
				tobedelete=newArray(obj_end);
				Array.fill(tobedelete,0);
				for(j=0;j<=obj_middle-1;j++)
				{ //loop on each "old" object from previous threshold
					newImage("Locations", "8-bit black", x, y, z);
					Ext.Manager3D_Select(j);
					Ext.Manager3D_FillStack(255, 255, 255); //make one "old object" black in a new image
					p=0;
					count=newArray(new_obj);
					Array.fill(count,0);
					q=0;
					selectWindow("Locations");
					for(k=obj_middle;k<=obj_end-1;k++)
					{ //loop on each new object identified by current threshold
						Ext.Manager3D_Select(k);
						Ext.Manager3D_Quantif3D(k,"Mean",mean);
						if(mean>=253)
						{ //for new objects completely embedded in old objects (should be like this when increasing threshold
							p=p+1;
							count[q]=k;
						}
						q=q+1;
						Ext.Manager3D_DeselectAll();
					}
					if((p==0)|(p==1))
					{ //if only one or no object is zeroed ie the big object from previous round simply gets smaller or disappeared
						for(q=0;q<=new_obj-1;q++)
						{
							if(count[q]!=0)
							{
								tobedelete[count[q]]=1;
							}
						}
					}
					if(p>1)
					{ //if more than one object are zeroed ie the big object was split in this round
						Ext.Manager3D_Measure3D(j,"Vol",vol);
						//
						//adjust the volume threshold, by default unit vol is returned 
						//
						if(vol>=200)
						{
						tobedelete[j]=1;
						}
					}
					selectWindow("Locations");
					close();
					Ext.Manager3D_DeselectAll();
				}
				for(n=obj_middle;n<=obj_end-1;n++)
				{ //delete very small objects (likely background) and very big objects (mesenchyme or periderm)
					Ext.Manager3D_Measure3D(n,"Vol",vol);
					//
					//by default unit vol is used
					//
					if((vol<10)|(vol>10000))
					{ 
						Ext.Manager3D_Select(n);
						tobedelete[n]=1;
					}	
				}
				Ext.Manager3D_DeselectAll();
				for(s=0;s<=obj_end-1;s++)
				{
					if(tobedelete[s]==1)
					{
						Ext.Manager3D_Select(s);
						Ext.Manager3D_MultiSelect();
					}	
				}
				Ext.Manager3D_Delete();
				Ext.Manager3D_Count(nb_obj);
				obj_middle=nb_obj;
				print("new_obj_middle",obj_middle);
				i=i-2.5;
			}
			Ext.Manager3D_Count(nb_obj);
			seg_obj=nb_obj;
			print("seg_obj",seg_obj);
			//test1
			/*Ext.Manager3D_SelectAll();
			Ext.Manager3D_Save(dir3+name+"Objects 1.zip");
			Ext.Manager3D_DeselectAll();
			for(co=seg_obj-1;co>=0;co--)
			{
				selectWindow("Smoothed");
				run("RGB Color");
				r=255*random;
				g=255*random;
				b=255*random;
				Ext.Manager3D_Select(co);
				Ext.Manager3D_FillStack(r, g, b);
				Ext.Manager3D_DeselectAll();
			}
			selectWindow("Smoothed");
			saveAs("Tiff",dir2+name+"coloured objects 1.tif");*/
			//delete non-central objects
			Ext.Manager3D_DeselectAll();
			for(mi=seg_obj-1;mi>=0;mi--)
			{
				Ext.Manager3D_Centroid3D(mi,cx,cy,cz);
				if((cx<50)|(cx>150)|(cy<50)|(cy>150))
				{			
					Ext.Manager3D_Select(mi);
					Ext.Manager3D_MultiSelect();
				}	
			}
			Ext.Manager3D_Delete();
			Ext.Manager3D_Count(nb_obj);
			seg_obj=nb_obj;
			print("seg_obj_central",seg_obj); 
			Ext.Manager3D_DeselectAll();
			//delete objects at b catenin voids
			for(bl=seg_obj-1;bl>=0;bl--)
			{			
				selectWindow("smoothed blue");
				Ext.Manager3D_Quantif3D(bl,"Max",max);
				if(max<=bluethreshold)
				{
					Ext.Manager3D_Select(bl);
					Ext.Manager3D_Delete();
				}	
			}
			Ext.Manager3D_Count(nb_obj);
			seg_obj=nb_obj;
			print("seg_obj_DAPI_positive",seg_obj);
			Ext.Manager3D_DeselectAll();
			//delete false objects
			for(su=seg_obj-1;su>=0;su--)
			{			
				//selectWindow("Smoothed.tif");
				Ext.Manager3D_Measure3D(su,"DCMean",r);
				if(isNaN(r))
				{
					Ext.Manager3D_Select(su);
					Ext.Manager3D_MultiSelect();
				}	
			}
			Ext.Manager3D_Delete();
			Ext.Manager3D_Count(nb_obj);
			seg_obj=nb_obj;
			print("final obj",seg_obj);
			Ext.Manager3D_DeselectAll();
			for(re=0;re<seg_obj;re++)
			{
				Ext.Manager3D_Select(re);
				Ext.Manager3D_Rename(re);
				Ext.Manager3D_DeselectAll();
			}
			Ext.Manager3D_SelectAll();
			Ext.Manager3D_Save(dir3+name+"Objects.zip");
			//colour the objects
			for(co=seg_obj-1;co>=0;co--)
			{
				selectWindow("Smoothed");
				run("RGB Color");
				r=255*random;
				g=255*random;
				b=255*random;
				Ext.Manager3D_Select(co);
				Ext.Manager3D_FillStack(r, g, b);
				Ext.Manager3D_DeselectAll();
			}
			selectWindow("Smoothed");
			saveAs("Tiff",dir2+name+"coloured objects.tif");
			while (nImages>0) 
			{
				title = getTitle();
				selectWindow(title);
				run("Close");
			}
			//selectWindow("smoothed blue");
			//close();
			Ext.Manager3D_Close();
		}
	}
	else
	{
		list_finished=getFileList(dir2);
		for (res=list_finished.length; res<list1.length; res++)
		{
			open(dir1+list1[res]);
			name=getTitle();
			width=getWidth();
			height=getHeight();
			rename("stack");
			run("Split Channels");
			selectWindow("C1-stack");
			Z=nSlices();
			intensityBlue=newArray(Z);
			selectWindow("C1-stack");
			run("Smooth (3D)", "method=Gaussian sigma=1.000 use");
			selectWindow("Smoothed");
			//find dapi channel threshold
			//setSlice(Z/2);
			for (j=0; j<Z; j++)
			{
				selectWindow("Smoothed");
				setSlice(j+1);
				makeRectangle(0, 0, width, height);
				getRawStatistics(nPixels, mean, min, max, std, histogram);
				intensityBlue[j]=max;
			}
			for (j=0; j<Z; j++)
			{
				if(intensityBlue[j]>50)
				{
					top=j;
					j=Z;
				}
			}
			for (j=Z-1; j>=0; j--)
			{
				if(intensityBlue[j]>50)
				{
					bottom=j;
					j=0;
				}
			}
			setSlice((top+bottom)/2);
			setAutoThreshold("Default dark");
			getThreshold(lower, upper);
			bluethreshold=lower;
			selectWindow("Smoothed");
			rename("smoothed blue");
			selectWindow("C1-stack");
			close();
			//find local minima with iterative thresholding and object counter
			imageCalculator("Add create stack", "C2-stack","C3-stack");
			rename("stack");
			selectWindow("C2-stack");
			close();
			selectWindow("C3-stack");
			close();
			selectWindow("stack");
			x=getHeight();
			y=getWidth();
			//run("Slice Keeper", "first="+top+"last="+bottom+"increment=1");//delete unnecessary slices
			//selectWindow("stack");
			//close();
			//selectWindow("stack kept stack");
			//rename("stack");
			z=nSlices();
			run("Invert", "stack");
			rename("stack");
			run("Smooth (3D)", "method=Gaussian sigma=1.000 use");
			selectWindow("stack");
			close();
			selectWindow("Smoothed");
			//run("Enhance Contrast...", "saturated=1 normalize process_all");
			//run("Enhance Contrast", "saturated=1 normalize update normalize_all");
			//as can be problems if no objects identified, stamp bottom slice with a 10x10 square in corner so not to interfer with subsequent processing
			setSlice(1);
			setColor(255,255,255);
			fillRect(0,0,10,10);
			areaz=z*x*y;
			i=60;
			threshold=220-i;
			print("threshold",threshold);
			i=i-2.5;
			selectWindow("Smoothed");
			run("3D Objects Counter", "threshold="+threshold+" slice=1 min.=100 max.="+areaz+" objects");
			selectWindow("Objects map of Smoothed");
			run("3D Manager");
			Ext.Manager3D_AddImage();
			Ext.Manager3D_Count(nb_obj);
			print("number of object",nb_obj);
			selectWindow("Objects map of Smoothed");
			close();
			obj_end=nb_obj;
			obj_middle=obj_end;
			//I used 5 in threshold stepping because I found the intensity still varies a bit after smoothing
			//5 misses some information so need to try smaller steps
			while(i>=0)
			{
				threshold=225-i;
				print("threshold",threshold);
				selectWindow("Smoothed");
				run("3D Objects Counter", "threshold="+threshold+" slice=1 min.=100 max.="+areaz+" objects");
				selectWindow("Objects map of Smoothed");
				Ext.Manager3D_AddImage();
				Ext.Manager3D_Count(nb_obj); 
				print("total number of objects",nb_obj);
				selectWindow("Objects map of Smoothed");
				close();
				obj_end=nb_obj; //end of object list
				new_obj=obj_end-obj_middle; //added new objects at this threshold
				tobedelete=newArray(obj_end);
				Array.fill(tobedelete,0);
				for(j=0;j<=obj_middle-1;j++)
				{ //loop on each "old" object from previous threshold
					newImage("Locations", "8-bit black", x, y, z);
					Ext.Manager3D_Select(j);
					Ext.Manager3D_FillStack(255, 255, 255); //make one "old object" black in a new image
					p=0;
					count=newArray(new_obj);
					Array.fill(count,0);
					q=0;
					selectWindow("Locations");
					for(k=obj_middle;k<=obj_end-1;k++)
					{ //loop on each new object identified by current threshold
						Ext.Manager3D_Select(k);
						Ext.Manager3D_Quantif3D(k,"Mean",mean);
						if(mean>=253)
						{ //for new objects completely embedded in old objects (should be like this when increasing threshold
							p=p+1;
							count[q]=k;
						}
						q=q+1;
						Ext.Manager3D_DeselectAll();
					}
					if((p==0)|(p==1))
					{ //if only one or no object is zeroed ie the big object from previous round simply gets smaller or disappeared
						for(q=0;q<=new_obj-1;q++)
						{
							if(count[q]!=0)
							{
								tobedelete[count[q]]=1;
							}
						}
					}
					if(p>1)
					{ //if more than one object are zeroed ie the big object was split in this round
						Ext.Manager3D_Measure3D(j,"Vol",vol);
						if(vol>=200)
						{
						tobedelete[j]=1;
						}
					}
					selectWindow("Locations");
					close();
					Ext.Manager3D_DeselectAll();
				}
				for(n=obj_middle;n<=obj_end-1;n++)
				{ //delete very small objects (likely background) and very big objects (mesenchyme or periderm)
					Ext.Manager3D_Measure3D(n,"Vol",vol);
					if((vol<10)|(vol>10000))
					{ //from experience 2000 is a good value
						Ext.Manager3D_Select(n);
						tobedelete[n]=1;
					}	
				}
				Ext.Manager3D_DeselectAll();
				for(s=0;s<=obj_end-1;s++)
				{
					if(tobedelete[s]==1)
					{
						Ext.Manager3D_Select(s);
						Ext.Manager3D_MultiSelect();
					}	
				}
				Ext.Manager3D_Delete();
				Ext.Manager3D_Count(nb_obj);
				obj_middle=nb_obj;
				print("new_obj_middle",obj_middle);
				i=i-2.5;
			}
			Ext.Manager3D_Count(nb_obj);
			seg_obj=nb_obj;
			print("seg_obj",seg_obj);
			//delete non-central objects
			Ext.Manager3D_DeselectAll();
			for(mi=seg_obj-1;mi>=0;mi--)
			{
				Ext.Manager3D_Centroid3D(mi,cx,cy,cz);
				if((cx<50)|(cx>150)|(cy<50)|(cy>150))
				{			
					Ext.Manager3D_Select(mi);
					Ext.Manager3D_MultiSelect();
				}	
			}
			Ext.Manager3D_Delete();
			Ext.Manager3D_Count(nb_obj);
			seg_obj=nb_obj;
			print("seg_obj_central",seg_obj); 
			Ext.Manager3D_DeselectAll();
			//delete objects at b catenin voids
			print("bluethreshold", bluethreshold);
			for(bl=seg_obj-1;bl>=0;bl--)
			{			
				selectWindow("smoothed blue");
				Ext.Manager3D_Quantif3D(bl,"Max",max);
				print("Max blue", max);
				if(max<=bluethreshold)
				{
					Ext.Manager3D_Select(bl);
					Ext.Manager3D_Delete();
				}	
			}		
			Ext.Manager3D_Count(nb_obj);
			seg_obj=nb_obj;
			print("seg_obj_DAPI_positive",seg_obj);
			Ext.Manager3D_DeselectAll();
			for(su=seg_obj-1;su>=0;su--)
			{			
				//selectWindow("Smoothed.tif");
				Ext.Manager3D_Measure3D(su,"DCMean",r);
				if(isNaN(r))
				{
					Ext.Manager3D_Select(su);
					Ext.Manager3D_MultiSelect();
				}	
			}
			Ext.Manager3D_Delete();
			Ext.Manager3D_Count(nb_obj);
			seg_obj=nb_obj;
			print("final obj",seg_obj);
			Ext.Manager3D_DeselectAll();
			for(re=0;re<seg_obj;re++)
			{
				Ext.Manager3D_Select(re);
				Ext.Manager3D_Rename(re);
				Ext.Manager3D_DeselectAll();
			}
			Ext.Manager3D_SelectAll();
			Ext.Manager3D_Save(dir3+name+"Objects.zip");
			//colour the objects
			for(co=seg_obj-1;co>=0;co--)
			{
				selectWindow("Smoothed");
				run("RGB Color");
				r=255*random;
				g=255*random;
				b=255*random;
				Ext.Manager3D_Select(co);
				Ext.Manager3D_FillStack(r, g, b);
				Ext.Manager3D_DeselectAll();
			}
			selectWindow("Smoothed");
			saveAs("Tiff",dir2+name+"coloured objects.tif");
			while (nImages>0) 
			{
				title = getTitle();
				selectWindow(title);
				run("Close");
			}
			//selectWindow("smoothed blue");
			//close();
			Ext.Manager3D_Close();
		}
	}
}

function Smooth(array,S,F,W){
	//given an array, a start (S) and end (E) point (for cases where image in Z stops mid 
	//stack and therefore do not want to smooth across boundary) and a smoothing width (+-W)
	L=array.length;
	smooth=newArray(L);
	//smooth array over filter width W each side
	//run from S+W to F-W so as not to smooth over boundary
	for (i=S+W; i<=F-W; i++) {			
		sum=0;
		for (j=i-W; j<=i+W; j++) {
			sum=sum+array[j];
			}
		smooth[i]=sum/((2*W)+1);
		}
	//Ends of list so not have blank terms
	//Smooth from list end (S or F) to W list terms along (ie filter is asymmetric in shape)
	for (i=S; i<S+W; i++) {
		sum=0;
		for (j=S; j<=i+W; j++) {
			sum=sum+array[j];
			}
		smooth[i]=sum/(i-S+(W+1));
		}
	for (i=F-W+1; i<=F; i++) {	
		sum=0;
		for (j=i-W; j<=F; j++) {
			sum=sum+array[j];
			}
		smooth[i]=sum/(F+(W+1)-i);
		}	
	return smooth;
	}

function Gradient(array,S,F,W,D){
	//Calculate gradient of an array by measuring increase in array value over a window of 
	//W width, starting at S and ending at F. Also can put "dummy points" at ends where extrapolate 
	//the first/last term of the array over a set of point at the end
	//also tried using a Taylor expansion to calculate first, second third etc derivatives, 
	//which ultimately gave a very similar calculation, but needed very wide area over which 
	//to calculate gradient to reduce noise, therefore stuck with this method
	L=array.length;
	gradient=newArray(L);
	//calculate gradient over filter width W each side but leaving D dummy points (ie copies 
	//of first true point) at each end	
	for (i=(S+D)+W; i<=(F-D)-W; i++) {	
		gradient[i]=(array[i+W]-array[i-W])/(2*W);		
		}	
	//Ends of list so not have blank terms
	for (i=(S+D)+1; i<(S+D)+W; i++) {
		gradient[i]=(array[i+(i-(S+D))]-array[S+D])/((i-(S+D))*2);	
		}
	for (i=(F-D)-W+1; i<=(F-D)-1; i++) {
		gradient[i]=(array[(F-D)]-array[i-((F-D)-i)])/(((F-D)-i)*2);	
		}
	//fill in dummy terms at ends	
	//if calculating a second or third derivative, not want to calculate it over the extrapolated 
	//point at the end, so put in dummy terms over which gradient at ends is extrapolated
	for (i=S+D; i>=S; i--) {
		gradient[i]=gradient[i+1];	
		}
	for (i=F-D; i<=F; i++) {
		gradient[i]=gradient[i-1];	
		}
	return gradient;
	}

	

function TimerA(operation,new){
	if(new==true){
		run("Text Window...", "name=[Progress] width=35 height=3 menu");
		}
	else{
		print("[Progress]","\\Update:");
		}
	print("[Progress]",operation);
	print("[Progress]","\nTime remaining: calculating...");
	getDateAndTime(year, month, dayOfWeek, dayOfMonth, hour, minute, second, msec);
	time=dayOfMonth*24*60*60*1000+hour*60*60*1000+minute*60*1000+second*1000+msec;
	return time;
	}

function TimerB(operation,time1,completed,total){
	getDateAndTime(year, month, dayOfWeek, dayOfMonth, hour, minute, second, msec);
	time2=dayOfMonth*24*60*60*1000+hour*60*60*1000+minute*60*1000+second*1000+msec;
	if(time2<time1){
		time2=time2+30*24*60*60*1000;
		}
	time=time2-time1;
	esttime_msec=(time/completed)*(total-completed);
	esttime_hour=((esttime_msec/1000)/60)/60;
	esthour=floor(esttime_hour);
	esttime_min=(esttime_hour-esthour)*60;
	estmin=floor(esttime_min);
	esttime_sec=(esttime_min-estmin)*60;
	estsec=floor(esttime_sec);	
	timeremain=""+esthour+" hour "+estmin+" min "+estsec+" sec";
	if (((completed/total)>=0)&((completed/total)<0.1)){
		bar="--------------------";
		}
	if (((completed/total)>=0.05)&((completed/total)<0.1)){
		bar="+-------------------";
		}
	if (((completed/total)>=0.1)&((completed/total)<0.15)){
		bar="++------------------";
		}
	if (((completed/total)>=0.15)&((completed/total)<0.2)){
		bar="+++-----------------";
		}
	if (((completed/total)>=0.2)&((completed/total)<0.25)){
		bar="++++----------------";
		}
	if (((completed/total)>=0.25)&((completed/total)<0.3)){
		bar="+++++---------------";
		}
	if (((completed/total)>=0.3)&((completed/total)<0.35)){
		bar="++++++--------------";
		}
	if (((completed/total)>=0.35)&((completed/total)<0.4)){
		bar="+++++++-------------";
		}
	if (((completed/total)>=0.4)&((completed/total)<0.45)){
		bar="++++++++------------";
		}
	if (((completed/total)>=0.45)&((completed/total)<0.5)){
		bar="+++++++++-----------";
		}
	if (((completed/total)>=0.5)&((completed/total)<0.55)){
		bar="++++++++++----------";
		}
	if (((completed/total)>=0.55)&((completed/total)<0.6)){
		bar="+++++++++++---------";
		}
	if (((completed/total)>=0.6)&((completed/total)<0.65)){
		bar="++++++++++++--------";
		}
	if (((completed/total)>=0.65)&((completed/total)<0.7)){
		bar="+++++++++++++-------";
		}
	if (((completed/total)>=0.7)&((completed/total)<0.75)){
		bar="++++++++++++++------";
		}
	if (((completed/total)>=0.75)&((completed/total)<0.8)){
		bar="+++++++++++++++-----";
		}
	if (((completed/total)>=0.8)&((completed/total)<0.85)){
		bar="++++++++++++++++----";
		}
	if (((completed/total)>=0.85)&((completed/total)<0.9)){
		bar="+++++++++++++++++---";
		}
	if (((completed/total)>=0.9)&((completed/total)<0.95)){
		bar="++++++++++++++++++--";
		}
	if (((completed/total)>=0.95)&((completed/total)<1)){
		bar="+++++++++++++++++++-";
		}
	if ((completed/total)==1){
		bar="++++++++++++++++++++";
		}		
	print("[Progress]", "\\Update:");
	print("[Progress]",operation);
	print("[Progress]","\nTime remaining: "+timeremain);
	print("[Progress]","\n"+bar);
	}

function TimerC(operation){
	print("[Progress]","\\Update:");
	print("[Progress]",operation);
	print("[Progress]","\nTime remaining: Finished");
	print("[Progress]","\n++++++++++++++++++++");
	}


function Power(A,B){
	//couldn't find how to raise to a power in ImageJ (but sure it's there somewhere) so wrote 
	//macro to do it
	I=1;
	for(i=0;i<B;i++){
		I=I*A;
		}
	return I;
	}

function NumberOfDigits(N){	
	//input a number N, returns number of digits that make the number D
	A=0;
	i=1;
	while (A>=0){
		A=N-Power(10,i);
		i=i+1;
		}
	D=i-1;
	return D;
}


function Digits(I,N){
	// given a number I, returns it as a string N digits long by appending 0 to the start
	NC=I;
	for(i=1;i<N;i++){
		if((I-Power(10,i))<0){
			NC="0"+NC;
			}
		}
	return NC;
	}
