%calculate the normal vector to a plane determined by three points...
%and return the angle value 
function angles=anglemeasurement(k,closest,Vxpix,Vypix,Vzpix)
a1=zeros(1,3);
a2=zeros(1,3);
a3=zeros(1,3);
b1=zeros(1,3);
b2=zeros(1,3);
b3=zeros(1,3);
a1(1)=closest(1,2)-closest(3,2);
a1(2)=closest(1,3)-closest(3,3);
a1(3)=closest(1,4)-closest(3,4);
b1(1)=closest(2,2)-closest(3,2);
b1(2)=closest(2,3)-closest(3,3);
b1(3)=closest(2,4)-closest(3,4);
a2(1)=closest(4,2)-closest(6,2);
a2(2)=closest(4,3)-closest(6,3);
a2(3)=closest(4,4)-closest(6,4);
b2(1)=closest(5,2)-closest(6,2);
b2(2)=closest(5,3)-closest(6,3);
b2(3)=closest(5,4)-closest(6,4);
a3(1)=closest(7,2)-closest(9,2);
a3(2)=closest(7,3)-closest(9,3);
a3(3)=closest(7,4)-closest(9,4);
b3(1)=closest(8,2)-closest(9,2);
b3(2)=closest(8,3)-closest(9,3);
b3(3)=closest(8,4)-closest(9,4);
C1=cross(a1,b1);
C2=cross(a2,b2);
C3=cross(a3,b3);
C_average=(1/3)*(C1+C2+C3);
if C_average(1)<0.1 | C_average(2)<0.1 | C_average(3)<0.1
    C_average=(1/2)*(C1+C2);
end
%calculate the angle between the object major axis and the plane vector
lamina=C_average/norm(C_average);
xyplane=[0 0 1];
object=zeros(1,3);
object(1)=Vxpix(k);
object(2)=Vypix(k);
object(3)=Vzpix(k);
angle1 = atan2(norm(cross(lamina,object)),dot(lamina,object));
object_to_lamina=90-(angle1*180/3.14);
if object_to_lamina<0
    object_to_lamina=abs(object_to_lamina);
end
angle2 = atan2(norm(cross(xyplane,object)),dot(xyplane,object));
object_to_xyplane=90-(angle2*180/3.14);
if object_to_xyplane<0
    object_to_xyplane=abs(object_to_xyplane);
end
angle3 = atan2(norm(cross(xyplane,lamina)),dot(xyplane,lamina));
lamina_to_xyplane=angle3*180/3.14;
if lamina_to_xyplane<0
    lamina_to_xyplane=abs(lamina_to_xyplane);
end
if lamina_to_xyplane>90
    lamina_to_xyplane=180-lamina_to_xyplane;
end
angles=zeros(1,3);
angles(1)=object_to_lamina;
angles(2)=object_to_xyplane;
angles(3)=lamina_to_xyplane;
end